﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="A1Builder.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide">
    <!-- Wrapper for slides -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active"> <img class="d-block w-100" src="images/slide-1.jpg" alt="First slide">
          <div class="carousel-caption">
            <h1>Dubai Creek Harbour <br>
              is at the heart of a vision of Dubai</h1>
            <p class="mt-5"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
            <p class="text-right mt-5 pt-5"><a href="projects-current.html" class="more-btn">More Details</a></p>
          </div>
        </div>
        <div class="carousel-item"> <img class="d-block w-100" src="images/slide-2.jpg" alt="Second slide">
          <div class="carousel-caption">
            <h1>Paving Work in Al Zorah,<br>
              a bustling seaside community)</h1>
            <p class="mt-5"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
            <p class="text-right mt-5 pt-5"><a href="projects-completed.aspx" class="more-btn">More Details</a></p>
          </div>
        </div>
        <div class="carousel-item"> <img class="d-block w-100" src="images/slide-3.jpg" alt="Third slide">
          <div class="carousel-caption">
            <h1>Al Ain Hospital <br>
              with 35+ medical departments</h1>
            <p class="mt-5"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
            <p class="text-right mt-5 pt-5"><a href="projects-current.aspx" class="more-btn">More Details</a></p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
    <!-- Controls -->
  </div>
</div>

<div class="container-fluid welcome-content">
  <div class="container">
    <h3 class="text-center py-4">Projects In Progress</h3>
    <div class="row">
      <div class="col-md-4 row-block-3"> <a href="projects-current.aspx"><img src="http://www.allenshariff.com/wp-content/uploads/2012/09/alAin4.jpg" border="0" class="img-fluid mx-auto" /></a>
        <h4>Al Ain Hospital</h4>
        <p></p>
      </div>
      <div class="col-md-4 row-block-3"> <a href="projects-current.aspx"><img src="https://www.emaar.com/en/Images/DCH-310x310_tcm223-103023.jpg" border="0" class="img-fluid mx-auto" /></a>
        <h4>Dubai Creek Harbour at The Lagoons</h4>
        <p></p>
      </div>
      <div class="col-md-4 row-block-3"> <a href="projects-current.aspx"><img src="https://www.emaar.com/en/Images/310x310-Harbour-Gate_tcm223-104242.jpg" border="0" class="img-fluid mx-auto" /></a>
        <h4>Dubai Creek Harbour</h4>
        <p></p>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid solutions-section">
  <div class="container">
    <h3 class="padding-10">Services</h3>
    <div class="row">
      <div class="col-md-4">
        <h4><span><i class="fa fa-star-o" aria-hidden="true"></i></span> Hard Landscaping</h4>
        <p>Hard landscaping consists of landscape features such as walkways, plazas and patios, which are
constructed with hard raw materials such as timber, concrete and granite. We use a wide variety of
these materials in the creation of numerous beautiful hardscape features. </p>
      </div>
      <div class="col-md-4">
        <h4><span><i class="fa fa-moon-o" aria-hidden="true"></i></span> Water Features</h4>
        <p>In landscape architecture and garden design, a water feature is one or more items from a range of fountains, pools, ponds, cascades, waterfalls, and streams. We design and build water features, streams and waterfalls in accordance with the conceptual requirement of clients.
</p>
      </div>
      <div class="col-md-4">
        <h4><span><i class="fa fa-sun-o" aria-hidden="true"></i></span> Pool Cleaning</h4>
        <p>Regular pool cleaning and maintaining chemical content of water to its required level is extremely important.  We carry out this this job with our specially trained team to ensure that the purity of the water is maintained. </p>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
    <h3 class="text-center py-4">Completed Projects</h3>
    <div class="row">
      <div class="col-lg-3 col-md-5 col-sm-12">
        <nav class="nav-sidebar">
          <ul class="nav tabs">
            <li><a class="active" href="#tab1" data-toggle="tab">Paving work on Al Zorah</a></li>
           <%-- <li class=""><a href="#tab2" data-toggle="tab">Project 2</a></li>
            <li class=""><a href="#tab3" data-toggle="tab">Project 3</a></li>
            <li class=""><a href="#tab4" data-toggle="tab">Project 4</a></li>
            <li class=""><a href="#tab5" data-toggle="tab">Project 5</a></li>
            <li class=""><a href="#tab6" data-toggle="tab">Project 6</a></li>--%>
          </ul>
        </nav>
      </div>
      <!-- tab content -->
      <div class="col-lg-9 col-md-7 col-sm-12 tab-area">
        <div class="tab-content">
          <div class="tab-pane active" id="tab1">
            <div id="pr1" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active"> <img class="img-fluid" src="http://www.alzorah.ae/media/2107/golf_estates_02.jpg" alt="First slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4008/dsc_9829-2.jpg" alt="Second slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4246/img_1369_01.jpg" alt="Third slide"> </div>
              </div>
            </div>
          </div>
          <div class="tab-pane text-style" id="tab2">
            <div id="pr2" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active"> <img class="img-fluid" src="http://www.allenshariff.com/wp-content/uploads/2012/09/alAin1.jpg" alt="First slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/2107/golf_estates_02.jpg" alt="First slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4008/dsc_9829-2.jpg" alt="Second slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4246/img_1369_01.jpg" alt="Third slide"> </div>
              </div>
            </div>
          </div>
          <div class="tab-pane text-style" id="tab3">
            <div id="pr3" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active"> <img class="img-fluid" src="http://www.allenshariff.com/wp-content/uploads/2012/09/alAin1.jpg" alt="First slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/2107/golf_estates_02.jpg" alt="First slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4008/dsc_9829-2.jpg" alt="Second slide"> </div>
                <div class="carousel-item"> <img class="img-fluid" src="http://www.alzorah.ae/media/4246/img_1369_01.jpg" alt="Third slide"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid learnmore-section">
  <div class="container">
    <h3 class="py-4">Subscribe to Newsletter</h3>
    <div class="row">
      <div class="col-lg-6">
        <p class="text-muted">AOne Builder puts the security of your email at a high priority. Our team will work hard to deliver good quality newsletters to your email id specified in the fields you have entered in subscription field. Thank you for your cooperation and understanding.
 </p>
      </div>
      <div class="col-lg-6">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Enter Your Email" aria-label="Enter Your Email">
          <span class="input-group-btn">
          <button class="btn btn-dark" type="button">Subscribe</button>
          </span> </div>
      </div>
    </div>
  </div>
</div>
</asp:Content>
