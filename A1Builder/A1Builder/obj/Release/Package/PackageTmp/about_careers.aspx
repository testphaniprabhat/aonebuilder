﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="about_careers.aspx.cs" Inherits="A1Builder.about_careers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Careers</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>We are on organization that takes pride in our workforce. Our work ethics and outstanding
standards create professional and secure environment in which we work. We provide the ideal
workplace for intuitive and hard working professionals. We are a growing organization with year
over year adding 100% additional workforce.</p>
<p>We provide correct tools and training schemes to equip our employee with experience and skills
necessary to promote their success.</p>
<hr>
<h4>We are actively seeking</h4>
<hr>
<p><strong>Project Engineers (Civil &amp; Swimming Pool &amp; Water features) Experience 6 years </strong> <span class="alert-secondary px-1">Location : Dubai </span></p>
<p>Graduate civil engineers or diploma holders with 6 years of experience in the construction
industry. Preference will be given to candidates having experience in landscaping and
swimming pools. Candidate should be able to manage projects independently and should
possess inter personal communication skills.</p>
<hr>
<p><strong>ForeMan (Civil &amp; Swimming Pool) Experience more than 3 years </strong> <span class="alert-secondary px-1">Location : Dubai </span></p>
<p>Diploma holders with 3 years of experience in the construction industry. Preference will be given
to candidates having experience in landscaping and swimming pools. Candidate should
possess inter personal communication skills.</p>

<div class="alert alert-info">Send your resume at <a href="mailto:" class="bold">careers@aonebuilders.com</a></div>
  </div>
</div>

</asp:Content>
