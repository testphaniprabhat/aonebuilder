﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="services-swimming-pools.aspx.cs" Inherits="A1Builder.services_swimming_pools" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Swimming Pools and Jacuzzis</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>AOne Builder specializes in design and execution of turkey projects based on the client's requirement (including free site surveys). Also we provide experts site visits for the completed villas, buildings and hotels to check and suggest the suitable pool and fountains as per site condition in accordance with the client's requirement. The new pool structure is made to last lifelong by the latest gunite equipment and requires skilled workforce to execute the same. In addition to the same our Director's 21 years of experience in the same field and highly experienced & professionally qualified AOne Builder staff provides satisfaction to the client beyond their expectations.
</p>
<hr>
    <div class="row">
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/j2-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/Jacuzzi-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/Untitled-3-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/Pool-Guniting-in-progress1-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/s11-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/Pool-Guniting-in-progress-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
    </div>
  </div>
</div>

</asp:Content>
