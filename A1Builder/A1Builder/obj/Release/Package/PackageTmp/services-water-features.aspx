﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="services-water-features.aspx.cs" Inherits="A1Builder.services_water_features" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Water Features</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>In landscape architecture and garden design, a water feature is one or more items from a range of fountains, pools, ponds, cascades, waterfalls, and streams. Before the 18th century they were usually powered by gravity.</p>
		<p>Since the 18th century, the majority of water features have been powered by pumps. In the past, the power source was sometimes a steam engine, but in modern features it is almost always powered by electricity.</p>
		<p>We design and build water features, streams and waterfalls in accordance with the conceptual requirement of clients.</p>
<hr>
    <div class="row">
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/f5-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/ff1-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/ff2-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/f1-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/f2-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/f4-300x225.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
    </div>
  </div>
</div>

</asp:Content>
