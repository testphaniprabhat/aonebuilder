﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="about-clients.aspx.cs" Inherits="A1Builder.about_clients" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Clients</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>AOne Building Contracting LLC has successfully completed numerous projects for wide range of
distinguished clients. We mainly cater to residential, commercial &amp; hospitality properties. Many of our
clients have chosen to continue working with us as their contractor.</p>
<hr>
    <div class="row aone-clients">
      <div class="col-sm-3">
	  <img src="http://proscapegroup.com/wp-content/uploads/2013/09/emaar-logo.jpg" class="img-thumbnail mx-auto" />
	  <h4>EMAAR</h4>
		</div>
      <div class="col-sm-3">
	  <img src="http://proscapegroup.com/wp-content/uploads/2013/09/al-naboodah.jpg" class="img-thumbnail mx-auto" />
	  <h4>AL Naboodah</h4>
		</div>
      <div class="col-sm-3">
	  <img src="http://proscapegroup.com/wp-content/uploads/2013/09/arabtec.jpg" class="img-thumbnail mx-auto" />
	  <h4>arabtec</h4>
		</div>
      <div class="col-sm-3">
	  <img src="http://proscapegroup.com/wp-content/uploads/2013/09/aldar.jpg" class="img-thumbnail mx-auto" />
	  <h4>ALDAR</h4>
		</div>
      <div class="col-sm-3">
	  <img src="http://proscapegroup.com/wp-content/uploads/2013/09/wade-adam.jpg" class="img-thumbnail mx-auto" />
	  <h4>Wade Adams</h4>
		</div>
      <div class="col-sm-3">
	  <img src="https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAr-AAAAJGZiOTc4NTViLTVhMDItNDJiZC1hMzZmLTZkMjFmMDQ2YmM4Mg.png" class="img-thumbnail mx-auto" />
	  <h4>Al Basti &amp; Mukhta</h4>
		</div>
      <div class="col-sm-3">
	  <img src="http://www.emrfm.com/jb-contents/files/pictures/HILLS_AND_FORT.png" class="img-thumbnail mx-auto" />
	  <h4>Hills &amp; Fort</h4>
		</div>
    </div>
  </div>
</div>

</asp:Content>
