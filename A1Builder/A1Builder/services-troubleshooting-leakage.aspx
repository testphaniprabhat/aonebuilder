﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="services-troubleshooting-leakage.aspx.cs" Inherits="A1Builder.services_troubleshooting_leakage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Troubleshooting Leakage</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
        <p>Troubleshooting leakage and solving the problem is our forte. We provide free survey and inspections based on your request subject to prior appointment. Our Director’s experience of over 20 years in pool industry makes us pool expert to identify and solve the problems in a professional way.</p>
<hr>
    <div class="row">
      <div class="col-sm-4">
	  <img src="http://newtechpools.com/new/wp-content/uploads/2015/05/Troubleshooting.jpg" class="img-thumbnail mx-auto my-2" />
		</div>
    </div>
  </div>
</div>


</asp:Content>
