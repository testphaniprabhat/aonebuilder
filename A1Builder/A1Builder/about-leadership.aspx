﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="about-leadership.aspx.cs" Inherits="A1Builder.about_leadership" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <!-- Testing for changes phani -->
    <!-- Testing for source control phani -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Leadership</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  <div class="container">
		  <div  class="row">
		  	<div class="col-md-2 col-sm-3 text-center"> <img src="images/amit.jpg" class="margin-top-bottom-10 max-width-100 img-thumbnail"></div>
			<div class="col-md-10 col-sm-9"><h4 class="margin-0 margin-bottom-10"><strong>Amit Rupchandani</strong> <small>- BE Mechanical , CEO,AOne Builders</small></h4> 
				
			<p>
			
<strong>Amit Rupchandani</strong> has served in various management positions including business strategy and execution with growth <br />
       in small to mid size companies for 19 years. Amit founded AONE Building Contracting LLC in 2016 with focus in exceeding  <br />
       the customers expectation with integrity in real estate development market. Amit has successfully completed the real estate <br />
       development in India that are based on sustainability. Amit has worked in UAE, Russia and India.He served as sales manager, 
       marketing manager and managing director in different companies with strong exposure to the international market such as Middle
       East, Africa, Latin America, South East Asia since 2001. He has also architect, design, built and managed complex technology
       platform in multicultural environment with successful execution by exceeding customer budget and delivery time. Amit has attended
       several  international building and technology conferences to keep him ahead of the market curve.<strong> Mr. Rupchandani</strong>
       has a degree in mechanical engineering from LD engineering college (Gujarat University),Ahmedabad, India, with focus in business 
       processes and service delivery. <strong> Mr. Rupchandani</strong> is deeply engaged in transforming the design and construction 
       practice and planning to make them more relevant, effective and environmentally conscious.<br /><br />


      <strong>Mr. Rupchandani</strong> is highly organized and client-focused, having outstanding skills in interacting with clients,<br />
       understanding their requirements and accordingly devising customized solutions,thereby maintaining complete client satisfaction
       and creating repeat business opportunities; <br /><br />

      <strong>Mr. Rupchandani</strong> is a Visionary & decisive leader, noted for sound, practical management style and excellent  <br /> 
      organization,communication, presentation & interpersonal skills; Proven ability to lead and motivate large cross-functional <br /> 
      and multi-cultural teams to maximize productivity, ensuring technical solutions meet business requirements.<br />


			</p>
		<p class="my-1 py-0"> 
		<a class="social-icon text-primary btn btn-light"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info btn btn-light"><i class="fa fa-twitter" aria-hidden="true"></i></a> </p>
			</div>
			</div>
	<hr>
		  <div  class="row">
		  	<div class="col-md-2 col-sm-3 text-center"> <img src="images/person-1.png" class="margin-top-bottom-10 max-width-100 img-thumbnail"></div>
			<div class="col-md-10 col-sm-9"><h4 class="margin-0 margin-bottom-10">Pankaj Sutaria <small>- Chairman of the Board</small></h4> 
			<p class="mb-0">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
		<p class="my-1 py-0"> 
		<a class="social-icon text-primary btn btn-light"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info btn btn-light"><i class="fa fa-twitter" aria-hidden="true"></i></a> </p>
			</div>
			</div>
  </div>
</div>

</asp:Content>
