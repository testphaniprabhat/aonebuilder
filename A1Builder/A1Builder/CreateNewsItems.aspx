﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="CreateNewsItems.aspx.cs" Inherits="A1Builder.CreateNewsItems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 116%;
        height: 300px;
    }
    .auto-style2 {
            width: 180px;
            font :bold;
            font-size :medium;
        }
        .auto-style6 {
            height: 25px;
        }
        .auto-style7 {
            height: 35px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
        <td colspan="2" class="auto-style6">
            <asp:Label ID="lblError" Font-Bold runat="server"></asp:Label>
        </td>
    </tr>
    
   <tr>
        <td class="auto-style2">Title Of The News Item:</td>
         <td>
        <asp:TextBox ID="Txtnews" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldTxtNews" runat="server" ControlToValidate="Txtnews" ErrorMessage="Enter News Item Title" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Display Content :</td>
        <td>
            <asp:TextBox ID="Txtcontent" runat="server" TextMode="MultiLine" Height="100px" Width="303px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldTxtcontent" runat="server" ControlToValidate="Txtcontent" ErrorMessage="Enter Display Content" ForeColor="Red"></asp:RequiredFieldValidator>
        </td>
    </tr>
    
    <tr>
        <td class="auto-style2">
            <asp:Button ID="createnewsitem" runat="server" Text="Create News Item" OnClick="createnewsitem_Click" />
        </td>
        <td style="text-align:">
            <asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click"/>
        </td>
    </tr>
    
</table>
</asp:Content>
