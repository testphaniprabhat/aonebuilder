﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace A1Builder
{
    public class Global : System.Web.HttpApplication
    {

        protected void Session_Start(object sender, EventArgs e)
        {
            Response.Redirect("~/index.aspx");
        }
    }
}