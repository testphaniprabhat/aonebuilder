﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using A1Builder.Models;

namespace A1Builder
{
    public partial class Newsdisplay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Displaynews();
        }

        protected void Displaynews()
        {
            try
            {
                var db = new aonebuilder();
                var newsdisp = (from q in db.Newsdisplay_Aonebuild
                                select new { q.Create_Ts, q.Newsdisplay_Title, q.Newsdisplay_Content }).ToList().OrderByDescending(x => x.Create_Ts);

                rptnews.DataSource = newsdisp;
                rptnews.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}