﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="projects-current.aspx.cs" Inherits="A1Builder.projects_current" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="slider">
  <div class="carousel slide-inner">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="text-right">
        <div class="container">
		<p class="my-0"> <a class="social-icon text-primary"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a class="social-icon text-info"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a class="social-icon text-danger"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a class="social-icon text-warning"><i class="fa fa-rss" aria-hidden="true"></i></a> </p>
</div>
      </div>
      <h3 class="margin-0">
        <div class="container">Current Projects</div>
      </h3>
    </div>
    <!-- Controls -->
  </div>
</div>
<div class="container-fluid client-section">
  
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center"> <a href="#" data-toggle="modal" data-target="#modalAlainhospital"><img src="http://www.allenshariff.com/wp-content/uploads/2012/09/alAin4.jpg" border="0" class="mx-auto max-height-300 img-thumbnail" /></a></div>
       <div class="col-md-8"> <h4>Al Ain Hospital</h4>
        <p>Allen & Shariff is providing Project Management services for the Al Ain Hospital Project. The design phase of Al Ain Hospital began in 2009, with construction completion scheduled for 2015. <br>
The Al Ain Hospital is a 358,000 sq. meter, 713 Bed Hospital, and will have underground parking facility for  1500 cars. Out of the 713 beds, 150 beds will be dedicated for rehabilitation. 
</p>
		<strong>Al Ain Hospital includes:</strong>
        <ul class="ml-4">
          <li>            Specialized trauma, orthopedics and sports-medicine units</li>
          <li>            40 bed critical care unit </li>
          <li>            High-tech cardiology unit</li>
        </ul>

      </div>
    </div>
	  <hr>
	  <div class="row">
      <div class="col-md-4 text-center"> <a href="#" data-toggle="modal" data-target="#modal-dubaicreekharbourlagoons"><img src="https://www.emaar.com/en/Images/DCH-310x310_tcm223-103023.jpg" class="mx-auto max-height-300 img-thumbnail" /></a></div>
       <div class="col-md-8"><h4>Dubai Creek Harbour at The Lagoons</h4>
        <p>Dubai Creek Residences will establish its presence at the creek as part of the island development, which captures the essence of harbour and marina lifestyle with its architecture and place making.</p>
		<strong>Amenities and Facilities:</strong>
        <ul class="ml-4">
          <li>            car parking</li>
          <li>            daycare facilities</li>
          <li>            children&rsquo;s swimming pool &amp; adult pool</li>
          <li>            deck&nbsp;</li>
          <li>            a state-of-the-art gymnasium &amp; yacht clubs</li>
          <li>            World-class marina &amp; retail podium</li>
          <li>            boulevard</li>
        </ul>
      </div>
      </div>
	  <hr>
      <div class="row">
      <div class="col-md-4 text-center"><a href="#" data-toggle="modal" data-target="#modal-dubaicreekharbour">
	  <img src="https://www.emaar.com/en/Images/310x310-Harbour-Gate_tcm223-104242.jpg" class="mx-auto max-height-300 img-thumbnail" /></a></div>
       <div class="col-md-8">
        <h4>Dubai Creek Harbour</h4>
        <p>Dubai Creek Harbour is at the heart of a bold new vision for Dubai. A fusion of creativity and innovation that will define the future of living and further enrich this great city. With spectacular cultural offerings, world class residences, shopping, amenities, offices and more, Dubai Creek Harbour represents the next frontier in contemporary life, work and play.

</p>
<p>In the heart of Dubai, surrounded by the ecosystem that is Dubai Creek Harbour, a vision is born. Built to exceed all limits, the new icon of the 21st century will become a part of your lifestyle. Designed by the world-renowned architect, Santiago Calatrava, the Dubai Creek Tower will be a unique timeless structure, where nature meets art, brought to life through unsurpassed technology and ambition.

</p>
      </div>
    </div>
  </div>
  
</div>

</asp:Content>
