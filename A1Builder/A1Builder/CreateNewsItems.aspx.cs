﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using A1Builder.Models;

namespace A1Builder
{
    public partial class CreateNewsItems : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }
      protected void insertnewsitem()
        {
            try
            {
                var db = new aonebuilder();
            var createnews = new Newsdisplay_Aonebuild();
            createnews.Newsdisplay_Title = Txtnews.Text;
            createnews.Newsdisplay_Content = Txtcontent.Text;
            createnews.Expiry_Flag = "N";
            createnews.Create_Ts= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
            createnews.Update_Ts= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));
            createnews.Newsdisplaysequence_ID = 1;
         // createnews.Newsdisplayimage_Name = "";
         // createnews.Newsdisplayimage_Path = "";
            db.Newsdisplay_Aonebuild.Add(createnews);
            db.SaveChanges();
            lblError.Text = "New newsitem added successfully";
            lblError.ForeColor = System.Drawing.Color.ForestGreen;
            }
           catch(Exception ex)
            {
                lblError.Text = ex.Message;
            }

        }

        protected void clear()
        {
            Txtnews.Text = "";
            Txtcontent.Text = "";
            lblError.Text = "";

        }
       
        protected void createnewsitem_Click(object sender, EventArgs e)
        {
            try
            {
                insertnewsitem();
            }
            catch (Exception ex)

            {
                lblError.Text = ex.Message;
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                clear();
            }
            catch (Exception ex)

            {
                lblError.Text = ex.Message;
            }
        }

    }
}